package com.games.boom.display;

/**
 * Created by maks on 15.08.2015.
 */
public enum Story implements DisplayResource {

    TARGET("target.txt"),
    VICTORY("victory.txt"),
    DEFEAT("defeat.txt");

    Story(String fileName) {
        this.fileName = fileName;
    }

    private final String STORY_FOLDER_PATH = "resources/story/";
    private String fileName;

    @Override
    public String getFilePath() {
        return STORY_FOLDER_PATH + fileName;
    }
}
