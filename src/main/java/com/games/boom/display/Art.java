package com.games.boom.display;

/**
 * Created by maks on 15.08.2015.
 */
public enum Art implements DisplayResource {

    LOGO("logo1.txt"),
    INTRO("intro.txt"),
    HAPPY_END("end1.txt"),
    BAD_END("end2.txt"),
    ROBOT_TINY("robot1.txt"),
    ROBOT_SMALL("robot2.txt"),
    ROBOT_MEDIUM("robot3.txt"),
    ROBOT_LARGE("robot4.txt"),
    ROBOT_BOSS("robot5.txt");

    Art(String fileName) {
        this.fileName = fileName;
    }

    private final String ART_FOLDER_PATH = "resources/ascii/";
    private String fileName;

    @Override
    public String getFilePath() {
        return ART_FOLDER_PATH + fileName;
    }
}
