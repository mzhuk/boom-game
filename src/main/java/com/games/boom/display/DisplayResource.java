package com.games.boom.display;

/**
 * Marker interface to aggregate printed objects like Art, Story, etc.
 * Created by maks on 16.08.2015.
 */
public interface DisplayResource {
    public String getFilePath();
}
