package com.games.boom.display;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by maks on 15.08.2015.
 */
public class GameMessages {

    private static final String MESSAGES_FILE_PATH = "resources/messages/messages.property";
    private Properties messages;
    
    public GameMessages() {
        messages = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(MESSAGES_FILE_PATH);
            messages.load(input);

        } catch (IOException e) {
            System.err.println("Failed to load game messages. Enemy spies must have destroyed the file. SOS!");
            throw new IllegalStateException();
        } finally {
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    System.err.println("Failed to close communications channel. Ts-s-sh! They might be still listening to us...");
                }
            }
        }
    }


    public String getMessage(MessageType type) {
        return messages.getProperty(type.toString());
    }

    public enum MessageType {

        GOFULLSCREEN,
        CONTINUE,
        GREETINGS,
        LEVELSELECT,
        NEXTLEVEL,
        MAKESHOT,
        STATS,
        EXIT,
        MEETROBOT,
        FINALBATTLE,
        DEFEAT,
        LEVELCLEAR;

    }
}
