package com.games.boom.business.game;

/**
 * Created by maks on 16.08.2015.
 */
public class GameOverException extends Exception {

    private static final long serialVersionUID = -37544741808501L;

    public GameOverException(String message) {
        super(message);
    }

}
