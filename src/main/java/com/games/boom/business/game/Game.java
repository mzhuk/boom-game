package com.games.boom.business.game;

import com.games.boom.business.robot.Robot;
import com.games.boom.business.robot.RobotFactory;

/**
 * Created by maks on 16.08.2015.
 */
public class Game {
    private static final String MSG_OUT_OF_SHOTS = "No more shots available.";
    private static final int ROBOT_TYPES_COUNT = Robot.RobotType.values().length;

    private int levelsCount;

    private int shotsCount;
    private int lastShotTarget;
    private int currentLevel;
    private Robot[] robots;

    // for future releases when robot strikes back
    private int livesCount;

    public Game(Difficulty difficulty) {
        this.levelsCount = calculateLevelsCount(difficulty);
        robots = initRobots(levelsCount, difficulty);

        this.shotsCount = calculateShotsCount(difficulty, robots);
        this.livesCount = difficulty.livesCount;
        currentLevel = 1;
    }

    private Robot[] initRobots(int levelsCount, Difficulty difficulty) {
        RobotFactory factory = new RobotFactory();
        Robot[] robots = new Robot[levelsCount];

        int initLevelsCout = 0;
        for (Robot.RobotType type : Robot.RobotType.values()) {
            for (int j = 0; j < difficulty.index && initLevelsCout < levelsCount; j++) {
                robots[initLevelsCout] = factory.produceRobot(type, difficulty);
                initLevelsCout++;
            }
        }

        return robots;
    }

    public Robot getLevelRobot() {
        return robots[currentLevel-1];
    }

    public int getLevelsCount() {
        return levelsCount;
    }

    public void makeShot(int target) throws GameOverException {
        this.shotsCount--;
        this.lastShotTarget = target;

        if (isGameOverSituation()) {
            throw new GameOverException(MSG_OUT_OF_SHOTS);
        }
    }

    public boolean isFinalBattle() {
        return currentLevel == levelsCount;
    }

    public void nextLevel() {
        currentLevel++;
        lastShotTarget = 0;
    }

    public int getRemainingShotsCount() {
        return shotsCount;
    }


    private int calculateLevelsCount(Difficulty difficulty) {
        return (ROBOT_TYPES_COUNT - 1) * difficulty.index + 1;
    }

    private int calculateShotsCount(Difficulty difficulty, Robot[] robots) {
        int totalFuelTanksCount = 0;
        for (Robot robot : robots) {
            totalFuelTanksCount += robot.getFuelTanksCount();
        }
        return (int) (totalFuelTanksCount * difficulty.getRatio());
    }

    private boolean isGameOverSituation() {
        return (!(isFinalBattle() && isLevelClear())
                && this.shotsCount <= 0);
    }

    public boolean isLevelClear() {
        return lastShotTarget > 0 &&
                getLevelRobot().isFuelTankExplode(lastShotTarget-1);
    }

    public enum Difficulty {
        EASY(1, 0.5, 3),
        NORMAL(2, 0.4, 2),
        HARD(3, 0.3, 1);

        private int index;
        private double ratio;
        private int livesCount;

        public int getIndex() {
            return index;
        }

        public double getRatio() {
            return ratio;
        }

        public static Difficulty getByIndex(int difficultyIndex) {
            for (Game.Difficulty level : Game.Difficulty.values()) {
                if (level.getIndex() == difficultyIndex) {
                    return level;
                }
            }
            return EASY;
        }

        Difficulty(int index, double ratio, int livesCount) {
            this.index = index;
            this.ratio = ratio;
            this.livesCount = livesCount;
        }
    }
}