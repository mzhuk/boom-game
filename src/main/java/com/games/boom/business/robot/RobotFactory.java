package com.games.boom.business.robot;

import com.games.boom.business.game.Game;

/**
 * Created by maks on 16.08.2015.
 */
public class RobotFactory {

    public Robot produceRobot(Robot.RobotType type, Game.Difficulty difficulty) {
        return new Robot(type, difficulty.getRatio());
    }
}
