package com.games.boom.business.robot;

import com.games.boom.display.Art;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by maks on 16.08.2015.
 */
public class Robot {
    private RobotType type;
    private boolean[] fuelTanks;

    public Robot(RobotType type, double loadFactor) {
        this.type = type;
        this.fuelTanks = initFuelTanks(type.fuelTanksCount, loadFactor);
    }

    public String getName() {
        return type.name;
    }

    public Art getArt() {
        return type.art;
    }

    public int getFuelTanksCount() {
        return fuelTanks.length;
    }

    private boolean[] initFuelTanks(int fuelTanksCount, double loadFactor) {
        boolean[] fuelTanks = new boolean[fuelTanksCount];

        Random random = new Random();
        for (int i = 0; i <= (fuelTanks.length * loadFactor); i ++) {
            fuelTanks[random.nextInt(fuelTanks.length)] = true;
        }

        return fuelTanks;
    }

    public boolean isFuelTankExplode(int target) {
        return fuelTanks[target];
    }

    public enum RobotType {
        TINY("Super Mega Enemy Destroyer", Art.ROBOT_TINY, 3),
        SMALL("Angry Cruel Destruction Maschine", Art.ROBOT_SMALL, 4),
        MEDIUM("Handsome Bobby-Boy", Art.ROBOT_MEDIUM, 5),
        LARGE("Nasty Natasha", Art.ROBOT_LARGE, 6),
        BOSS("De:)th Squ:)re", Art.ROBOT_BOSS, 10);

        private String name;
        private int fuelTanksCount;
        private Art art;

        RobotType(String name, Art art, int fuelTanksCount) {
            this.name = name;
            this.art = art;
            this.fuelTanksCount = fuelTanksCount;
        }
    }

    @Override
    public String toString() {
        return "Robot{" +
                "name='" + type.name + '\'' +
                ", fuelTanks=" + Arrays.toString(fuelTanks) +
                '}';
    }
}
