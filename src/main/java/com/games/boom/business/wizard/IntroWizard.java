package com.games.boom.business.wizard;

import com.games.boom.display.Art;
import com.games.boom.display.GameMessages;
import com.games.boom.io.ConsoleReader;
import com.games.boom.io.ConsoleWriter;
import static com.games.boom.display.GameMessages.MessageType.*;
/**
 * Created by maks on 15.08.2015.
 */
public class IntroWizard extends AbstractWizard {
    private GameMessages messages;

    public IntroWizard() {
        messages = new GameMessages();
    }

    public IntroWizard(ConsoleReader consoleReader, ConsoleWriter consoleWriter) {
        super(consoleReader, consoleWriter);
        messages = new GameMessages();
    }

    @Override
    public void run() {
        // go fullscreen
        consoleWriter.writeText(messages.getMessage(GOFULLSCREEN));
        consoleWriter.writeText(messages.getMessage(CONTINUE));
        consoleReader.readEnterKey();

        // slide text
        consoleWriter.slideResource(Art.INTRO, 3);

        // auth/reg
        consoleWriter.writeResource(Art.LOGO);
        consoleWriter.writeText(messages.getMessage(GREETINGS));
    }
}
