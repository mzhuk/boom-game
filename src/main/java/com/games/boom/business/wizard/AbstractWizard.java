package com.games.boom.business.wizard;

import com.games.boom.io.ConsoleReader;
import com.games.boom.io.ConsoleWriter;

/**
 * Created by maks on 15.08.2015.
 */
public abstract class AbstractWizard implements Wizard {
    protected ConsoleReader consoleReader;
    protected ConsoleWriter consoleWriter;

    public AbstractWizard() {}

    public AbstractWizard(ConsoleReader consoleReader, ConsoleWriter consoleWriter) {
        this.consoleReader = consoleReader;
        this.consoleWriter = consoleWriter;
    }

    @Override
    public void setIO(ConsoleReader consoleReader, ConsoleWriter consoleWriter) {
        this.consoleReader = consoleReader;
        this.consoleWriter = consoleWriter;
    }
}
