package com.games.boom.business.wizard;

import com.games.boom.business.account.AccountService;
import com.games.boom.business.account.AccountServiceException;
import com.games.boom.business.account.Account;
import com.games.boom.business.wizard.AbstractWizard;
import com.games.boom.io.ConsoleReader;
import com.games.boom.io.ConsoleWriter;

/**
 * Created by maks on 15.08.2015.
 */
public class AccountWizard extends AbstractWizard {
    private AccountService accountService;
    private Account account = null;

    public AccountWizard() {
    }

    public AccountWizard(ConsoleReader consoleReader, ConsoleWriter consoleWriter) {
        super(consoleReader, consoleWriter);
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void run() {

        while (null == account) {
            consoleWriter.writeText("\nChoose action:\n[1] Register\n[2] Login\n");
            int action = consoleReader.readInt(1, 2);

            try {
                if (ACTION.REGISTER.getAction() == action) {
                    account = register();
                } else if (ACTION.LOGIN.getAction() == action) {
                    account = login();
                } else {
                    consoleWriter.writeText("Invalid action specified. Please try again.");
                }

            } catch (AccountServiceException e) {
                consoleWriter.writeText(e.getMessage());
            }

            consoleWriter.writeText("\n");
        }

        accountService.setAccount(account);
    }

    /**
     * register new user account
     * @throws AccountServiceException
     */
    private Account register() throws AccountServiceException {

        consoleWriter.writeText("Enter username: ");
        String username = consoleReader.readString();

        if (accountService.verify(username)) {
            throw new AccountServiceException(String.format("Such \"%s\"  already exists. " +
                        "Please select another username.", username));
        }

        consoleWriter.writeText("Enter password: ");
        String password = consoleReader.readString();

        return accountService.register(username, password);
    }

    /**
     * login into existing user account
     * @throws AccountServiceException
     */
    private Account login() throws AccountServiceException {

        consoleWriter.writeText("Enter username: ");
        String username = consoleReader.readString();

        if (!accountService.verify(username)) {
            throw new AccountServiceException(String.format("User \"%s\" is not registered. " +
                    "Please verify username provided.", username));

        }

        consoleWriter.writeText("Enter password: ");
        String password = consoleReader.readString();

        return accountService.login(username, password);
    }

    private enum ACTION {
        REGISTER(1),
        LOGIN(2);

        private int action;

        public int getAction() {
            return action;
        }

        ACTION(int action) {
            this.action = action;
        }
    }
}
