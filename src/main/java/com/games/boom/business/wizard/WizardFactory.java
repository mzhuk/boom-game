package com.games.boom.business.wizard;

import com.games.boom.io.ConsoleReader;
import com.games.boom.io.ConsoleWriter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by maks on 15.08.2015.
 */
public class WizardFactory<W extends Wizard> {

    private Map<Class<W>, W> wizards = new HashMap<>();
    private ConsoleReader consoleReader;
    private ConsoleWriter consoleWriter;

    public WizardFactory(ConsoleReader consoleReader, ConsoleWriter consoleWriter) {
        this.consoleReader = consoleReader;
        this.consoleWriter = consoleWriter;
    }

    public W get(Class<W> wClass) throws IOException {

        if (wizards.containsKey(wClass)) {
            return wizards.get(wClass);
        }

        try {
            W wizard = wClass.newInstance();
            wizard.setIO(consoleReader, consoleWriter);

            wizards.put(wClass, wizard);
            return wizard;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IOException();
        }

    }
}
