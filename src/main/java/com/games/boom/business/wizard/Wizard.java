package com.games.boom.business.wizard;

import com.games.boom.io.ConsoleReader;
import com.games.boom.io.ConsoleWriter;

/**
 * Created by maks on 15.08.2015.
 */
public interface Wizard {
    void run();
    void setIO(ConsoleReader i, ConsoleWriter o);
}
