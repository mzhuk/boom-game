package com.games.boom.business.wizard;

import com.games.boom.business.game.Game;
import com.games.boom.business.game.GameOverException;
import com.games.boom.business.robot.Robot;
import com.games.boom.display.Art;
import com.games.boom.display.GameMessages;
import com.games.boom.display.Story;
import com.games.boom.io.ConsoleReader;
import com.games.boom.io.ConsoleWriter;

import static com.games.boom.display.GameMessages.MessageType.*;

/**
 * Created by maks on 16.08.2015.
 */
public class GameWizard extends AbstractWizard {

    private GameMessages messages;

    public GameWizard() {
        messages = new GameMessages();
    }

    public GameWizard(ConsoleReader consoleReader, ConsoleWriter consoleWriter) {
        super(consoleReader, consoleWriter);
        messages = new GameMessages();
    }

    @Override
    public void run() {
        // init
        consoleWriter.writeText(messages.getMessage(LEVELSELECT));
        int difficultyLevelIndex = consoleReader.readInt(1, 3);
        Game game = new Game(Game.Difficulty.getByIndex(difficultyLevelIndex));

        consoleWriter.writeResource(Story.TARGET);
        pause();

        try {
            for (int i = 0; i < game.getLevelsCount(); i++) {
                Robot robot = game.getLevelRobot();

                consoleWriter.writeResource(robot.getArt());
                consoleWriter.writeText(String.format(messages.getMessage(NEXTLEVEL), i + 1));
                consoleWriter.writeText(String.format(messages.getMessage(MEETROBOT), robot.getName()));

                if (game.isFinalBattle()) {
                    consoleWriter.writeText(messages.getMessage(FINALBATTLE));
                }

                // game process :)
                while (!game.isLevelClear()) {
                    consoleWriter.writeText(String.format(messages.getMessage(STATS), game.getRemainingShotsCount()));
                    makeShot(game);
                }
                consoleWriter.writeResource(Art.LOGO);
                consoleWriter.writeText(messages.getMessage(LEVELCLEAR));

                pause();
                game.nextLevel();
            }

            // victory
            consoleWriter.slideResource(Story.VICTORY, 1);
            consoleWriter.writeResource(Art.HAPPY_END);

        } catch (GameOverException e) {

            // defeat
            consoleWriter.slideResource(Story.DEFEAT, 1);
            consoleWriter.writeText(messages.getMessage(DEFEAT));
            pause();
            consoleWriter.writeResource(Art.BAD_END);
        }
    }

    private void pause() {
        consoleWriter.writeText(messages.getMessage(CONTINUE));
        consoleReader.readEnterKey();
    }

    private void makeShot(Game game) throws GameOverException {
        int fuelTanksCount = game.getLevelRobot().getFuelTanksCount();
        consoleWriter.writeText(String.format(messages.getMessage(MAKESHOT), fuelTanksCount));
        int target = consoleReader.readInt(1, fuelTanksCount);
        game.makeShot(target);
    }

}
