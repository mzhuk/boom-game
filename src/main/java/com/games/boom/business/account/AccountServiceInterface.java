package com.games.boom.business.account;

public interface AccountServiceInterface {

	/**
	 * Logs in the user, if the username exists and the password is correct.
	 */
	Account login(String username, String password)
			throws AccountServiceException;

	/**
	 * Registers a new Account, username have to be unique.
	 */
	Account register(String username, String password)
			throws AccountServiceException;

	/**
	 * Verifies account exists.
	 */
	boolean verify(String username);
}
