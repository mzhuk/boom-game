package com.games.boom.business.account;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

public class Account implements Serializable{

	private static final long serialVersionUID = 5889340229056466944L;

	private int id;
	private String username;
	private byte[] encryptedPassword;
	private String salt;
	private Date lastLogin;

	public Account(String username) {
		super();
		this.username = username;
		this.salt = UUID.randomUUID().toString();
	}
	
	public void updateLastLogin() {
		this.lastLogin = new Date();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public byte[] getEncryptedPassword() {
		return Arrays.copyOf(encryptedPassword, encryptedPassword.length);
	}

	public void setEncryptedPassword(byte[] password) {
		this.encryptedPassword = Arrays.copyOf(password, password.length);
	}

	public String getSalt() {
		return salt;
	}

	public Date getLastLogin() {
		return new Date(lastLogin.getTime());
	}

	@Override
	public int hashCode() {
		return  31 + id * 37 + username.hashCode() * 39;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;

		return id == other.id;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", username=" + username + "]";
	}
}
