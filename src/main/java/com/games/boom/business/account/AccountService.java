package com.games.boom.business.account;

import com.games.boom.persistence.Persistence;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

public class AccountService implements AccountServiceInterface {

    Persistence persistence = new Persistence();
    Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public Account login(String username, String password) throws AccountServiceException {

        Account account = persistence.findByUsername(username);
        if (null == account) {
            throw new AccountServiceException(String.format("User %s doesn't exist. Please verify username.", username));
        }

        byte[] encPassword = hash(password.toCharArray(), account.getSalt().getBytes(StandardCharsets.UTF_8));

        if (!Arrays.equals(encPassword, account.getEncryptedPassword())) {
            throw new AccountServiceException("The email and password you entered don't match. Please try again");
        }

        account.updateLastLogin();
        persistence.save(account);

        return account;
    }

    @Override
    public Account register(String username, String password) throws AccountServiceException {
        Account account = persistence.findByUsername(username);
        if (null != account) {
            throw new AccountServiceException(String.format("User %s already exists. Please select another username.", username));
        }

        account = new Account(username);
        account.setEncryptedPassword(hash(password.toCharArray(), account.getSalt().getBytes(StandardCharsets.UTF_8)));

        persistence.save(account);

        return account;
    }

    @Override
    public boolean verify(String username) {
        return null != persistence.findByUsername(username);
    }

    /**
     * Returns a salted and hashed password using the provided hash.<br>
     * Note - side effect: the password is destroyed (the char[] is filled with zeros)
     *
     * @param password the password to be hashed
     * @param salt     a 16 bytes salt, ideally obtained with the getNextSalt method
     *
     * @return the hashed password with a pinch of salt
     */
    public static byte[] hash(char[] password, byte[] salt)  {
        final int ITERATIONS = 10000;
        final int KEY_LENGTH = 256;

        PBEKeySpec spec = new PBEKeySpec(password, salt, ITERATIONS, KEY_LENGTH);
        Arrays.fill(password, Character.MIN_VALUE);
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            return skf.generateSecret(spec).getEncoded();

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new AssertionError("Error while hashing a password: " + e.getMessage(), e);
        } finally {
            spec.clearPassword();
        }
    }
}
