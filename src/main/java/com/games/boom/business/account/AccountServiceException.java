package com.games.boom.business.account;

public class AccountServiceException extends Exception {

	private static final long serialVersionUID = -37544741808500L;

	public AccountServiceException(String message) {
		super(message);
	}
	
}
