package com.games.boom;

import com.games.boom.business.account.AccountService;
import com.games.boom.business.wizard.AccountWizard;
import com.games.boom.business.wizard.GameWizard;
import com.games.boom.business.wizard.IntroWizard;
import com.games.boom.business.wizard.WizardFactory;
import com.games.boom.io.ConsoleReader;
import com.games.boom.io.ConsoleWriter;
import com.games.boom.io.GenericConsoleReader;
import com.games.boom.io.GenericConsoleWriter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class Application {

    public static void main(String[] args) throws IOException {
        ConsoleWriter storyTeller = new GenericConsoleWriter();
        ConsoleReader userListener = new GenericConsoleReader(new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8)));
        WizardFactory wizards = new WizardFactory(userListener, storyTeller);

        IntroWizard introWizard = (IntroWizard) wizards.get(IntroWizard.class);
        introWizard.run();

        AccountWizard accountWizard = (AccountWizard) wizards.get(AccountWizard.class);
        AccountService accountService = new AccountService();
        accountWizard.setAccountService(accountService);
        accountWizard.run();

        GameWizard gameWizard = (GameWizard) wizards.get(GameWizard.class);
        gameWizard.run();
    }

}
