package com.games.boom.io;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by maks on 15.08.2015.
 */
public class GenericConsoleReader implements ConsoleReader {
    BufferedReader reader;

    public GenericConsoleReader(BufferedReader reader) {
        this.reader = reader;
    }

    @Override
    public void readEnterKey() {
        readLine();
    }

    @Override
    public String readString() {
        String value = readLine();
        if (null == value || value.isEmpty()) {
            System.out.println("Empty input detected. Are you serious??? Enter a good value.");
            return readString();
        }
        return value;
    }

    @Override
    public int readInt(int min, int max) {
        final String MSG_WRONG_FORMAT = "Invalid input detected. What were you thinking about? We're not playing games here! " +
                "Enter a good value:";
        try {
            int value = Integer.parseInt(readLine());
            if (value < min || value > max) {
                throw new NumberFormatException();
            }

            return value;

        } catch (NumberFormatException e) {
            System.out.println(MSG_WRONG_FORMAT);
            return readInt(min, max);
        }
    }

    private String readLine() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            System.err.println("Our communication device is damaged. We're on fire... SOS! SOS! SOS!");
            return null;
        }
    }
}
