package com.games.boom.io;

/**
 * Created by maks on 15.08.2015.
 */
public interface ConsoleReader {
    void readEnterKey();
    String readString();
    int readInt(int min, int max);
}
