package com.games.boom.io;

import com.games.boom.display.DisplayResource;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

/**
 * Created by maks on 15.08.2015.
 */
public class GenericConsoleWriter implements ConsoleWriter {
    private final String EMPTY_SCREEN = getEmptyScreen();

    private void writeFrame() {
        System.out.println(EMPTY_SCREEN);
    }

    @Override
    public void writeText(String text) {
        System.out.print(text);
    }

    @Override
    public void writeResource(DisplayResource resource) {
        writeFrame();
        writeFileContent(resource.getFilePath(), 0);
    }

    @Override
    public void slideResource(DisplayResource resource, int frequency) {
        writeFrame();
        writeFileContent(resource.getFilePath(), frequency);
    }

    private void writeFileContent(String path, int frequency) {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8));
            String line = in.readLine();
            while (line != null) {
                System.out.println(line);
                sleep(frequency);

                line = in.readLine();
            }
            sleep(frequency*10);

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void sleep(int frequency) {
        try {
            if (frequency > 0) {
                Thread.sleep(TimeUnit.SECONDS.toMillis(1) / frequency);
            }
        } catch (InterruptedException e) {
            System.out.println("...");
        }
    }

    private final String getEmptyScreen() {
        final String NEW_LINE = "\n";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 100; i++) {
            sb.append(NEW_LINE);
        }
        return sb.toString();
    }
}
