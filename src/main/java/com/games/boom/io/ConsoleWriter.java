package com.games.boom.io;

import com.games.boom.display.DisplayResource;

/**
 * Created by maks on 15.08.2015.
 */
public interface ConsoleWriter {
    void writeText(String text);
    void writeResource(DisplayResource resource);
    void slideResource(DisplayResource resource, int frequency);
}
