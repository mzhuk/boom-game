package com.games.boom.persistence;

import com.games.boom.business.account.Account;

import java.util.HashMap;
import java.util.Map;

public class Persistence {

	private Map<String, Account> storage = new HashMap<>();

	public void save(Account account) {
		storage.put(account.getUsername(), account);
	}

	public Account findByUsername(String username) {
		return storage.get(username);
	}
}
