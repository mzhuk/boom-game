package com.games.boom;

import com.games.boom.business.account.AccountService;
import com.games.boom.business.account.AccountServiceException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class AccountServiceTest {

    AccountService accountService = new AccountService();

    @Before
    public void initialize() throws AccountServiceException {
        accountService.register("maks", "letmein");
    }

    @Test
    public void initialUserShouldBeRegistered() throws AccountServiceException {
       assertNotNull(accountService.login("maks", "letmein"));
    }

    @Test(expected = AccountServiceException.class)
    public void invalidUserShouldThrowException() throws AccountServiceException {
        accountService.login("not-maks", "letmein");
    }

    @Test(expected = AccountServiceException.class)
    public void invalidPasswordShouldThrowException() throws AccountServiceException {
        accountService.login("maks", "invalid-password");
    }


}
